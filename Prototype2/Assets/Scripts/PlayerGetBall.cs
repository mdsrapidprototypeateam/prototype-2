﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGetBall : MonoBehaviour {

    public BallRotation ball;
    PlayerMovement player;
    Rigidbody2D rb;
	float goalArea;

	// Use this for initialization
	void Start () {
        player = GetComponent<PlayerMovement>();
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {

	}

    private void OnTriggerEnter2D(Collider2D collider)
    {
		if (collider.tag == "goalArea") {
			if (collider.transform.parent.name == "GoalPlayer1") {
				goalArea = 1;
			} else {
				goalArea = 2;
			}
		}

        if (collider.tag == "Ball" && !ball)
        {
            if (player.canCatch || collider.transform.parent.GetComponent<BallBounce>().takenBy == EPlayerId.NONE)
            {
                Transform pai = collider.transform.parent;
                pai.parent = transform;
                ball = pai.GetComponent<BallRotation>();
                ball.shouldRotate = true;
                pai.position = transform.position;
                ball.rb.isKinematic = true;
                //ball.rb.isKinematic = false;
                ball.rb.velocity = Vector2.zero;
                ball.GetComponent<BallBounce>().takenBy = GetComponent<PlayerId>().playerId;
                ball.GetComponentInChildren<Collider2D>().gameObject.layer = 11;
				ball.transform.localPosition = new Vector2 (2000, 2000);
				Vector2 spawningPos = new Vector2();


                if (GetComponent<PlayerId>().playerId == EPlayerId.PLAYER1)
                {
                    if (GetComponent<PlayerMovement>().canCatch)
                    {
                        GameObject.Find("Player2").GetComponent<PlayerGetBall>().ball = null;
                    }
                    

					RaycastHit2D hit = Physics2D.Raycast (transform.localPosition, new Vector2 (transform.localPosition.x - ball.spinRadius, transform.localPosition.y), 1f);

					if (goalArea != 0) {
						if (goalArea == 1) {
							Transform goal = GameObject.Find ("GoalPlayer1").transform;
							spawningPos = (transform.position - goal.position).normalized * ball.spinRadius;
						} else {
							Transform goal = GameObject.Find ("GoalPlayer2").transform;
							spawningPos = (transform.position - goal.position).normalized * ball.spinRadius;
						}
					}

					else if (transform.position.x < -5.5f) {
						if (transform.position.y < -3) {
							spawningPos = new Vector2 (ball.spinRadius, 0);
						} else {
							spawningPos = new Vector2 (0, -ball.spinRadius);
						}
					} else if (transform.position.y > 5.5f) {
						if (transform.position.y < -3) {
							
							spawningPos = new Vector2 (-0.5f, 0.5f) * ball.spinRadius;
						} else {
							spawningPos = new Vector2 (0, ball.spinRadius);
						}
					} else {
						if (transform.position.y < -3) {
							
							spawningPos = new Vector2(-0.5f, 0.5f) * ball.spinRadius;
						} else {
							spawningPos = new Vector2(0.5f, -0.5f) * ball.spinRadius;
						}
					}

					ball.transform.localPosition = new Vector2 (0, 0);
					pai.localPosition = spawningPos;
					pai.right = pai.transform.position - transform.position;

					/*
					if (hit.transform.tag != "ballWall" && transform.position.y > -3)
                    {
						ball.transform.localPosition = new Vector2 (0, 0);
						pai.localPosition = new Vector2 (pai.localPosition.x - ball.spinRadius, pai.localPosition.y);
						pai.right = -transform.right;
                    } else
                    {
						ball.transform.localPosition = new Vector2 (0, 0);
                        pai.localPosition = new Vector2(pai.localPosition.x + ball.spinRadius, pai.localPosition.y);
                        pai.right = transform.right;
                    }*/
                }

                else
                {
                    if (GetComponent<PlayerMovement>().canCatch)
                    {
                        GameObject.Find("Player1").GetComponent<PlayerGetBall>().ball = null;
                    }

                    if (goalArea != 0) {
						if (goalArea == 1) {
							Transform goal = GameObject.Find ("GoalPlayer1").transform;
							spawningPos = (transform.position - goal.position).normalized * ball.spinRadius;
						} else {
							Transform goal = GameObject.Find ("GoalPlayer2").transform;
							spawningPos = (transform.position - goal.position).normalized * ball.spinRadius;
						}
					}


					else if (transform.position.x < -5.5f) {
						if (transform.position.y < -3) {
							spawningPos = new Vector2 (ball.spinRadius, 0);
						} else {
							spawningPos = new Vector2 (0, -ball.spinRadius);
						}
					} else if (transform.position.y > 5.5f) {
						if (transform.position.y < -3) {

							spawningPos = new Vector2 (-0.5f, 0.5f) * ball.spinRadius;
						} else {
							spawningPos = new Vector2 (0, ball.spinRadius);
						}
					} else {
						if (transform.position.y < -3) {

							spawningPos = new Vector2(-0.5f, 0.5f) * ball.spinRadius;
						} else {
							spawningPos = new Vector2(0.5f, -0.5f) * ball.spinRadius;
						}
					}

					ball.transform.localPosition = new Vector2 (0, 0);
					pai.localPosition = spawningPos;
					pai.right = pai.transform.position - transform.position;
                }


            }
        }
    }

	private void OnTriggerExit2D(Collider2D collider){
		if (collider.tag == "goalArea") {
			goalArea = 0;
		}
	}
}
