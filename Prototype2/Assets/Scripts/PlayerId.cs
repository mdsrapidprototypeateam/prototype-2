﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EPlayerId { PLAYER1, PLAYER2, NONE };
public enum EPlayerState
{
    NONE,
    STATE_IDLE,
    STATE_FOUND,
    STATE_GAME_INACTIVE,
    STATE_READY
}

public class PlayerId : MonoBehaviour
{
    private MyGameManager gameManager;
    public EPlayerId playerId = EPlayerId.PLAYER1;

    public EPlayerState playerState = EPlayerState.NONE;
    private EPlayerState mPlayerState = EPlayerState.NONE;

    public Sprite thisPlayerSprite;
    private SpriteRenderer m_srThisPlayerSpriteRenderer;

    public delegate void EPlayerStateChanged(EPlayerState value);
    public event EPlayerStateChanged OnEPlayerStateChangedEvent;

    public float allowedIdleTimeAfterFindingPlayer = 15f;
    public Vector3 startingPosition;

    void Start()
    {
        if ( !gameManager )
        {
            gameManager = (MyGameManager) GameObject.Find("GameManager").GetComponent<MyGameManager>();
        }

        //start to listening for changes on player state
        StartEPlayerStateCounter();

        m_srThisPlayerSpriteRenderer = GetComponentInChildren<SpriteRenderer>();
        m_srThisPlayerSpriteRenderer.sprite = thisPlayerSprite;

        playerState = EPlayerState.STATE_IDLE;

        startingPosition = transform.position;

    }


    void Update()
    {
        //listen for changes on player state
        GetEPlayerState();

        //Change state back to ready after returning to the players starting position
        ChangeBackToReadyAfterScore();

    }

    /// <summary>
    /// Auxiliary counter for tracking changes on EGameState property
    /// </summary>
    private void StartEPlayerStateCounter()
    {
        OnEPlayerStateChangedEvent += OnEPlayerStateChanged;
    }

    /// <summary>
    /// Get current EGameState public and private property values.
    /// Triggers event if they are not the same.
    /// /// </summary>
    public void GetEPlayerState()
    {
        if ( playerState != mPlayerState )
        {
            mPlayerState = playerState;

            if ( OnEPlayerStateChangedEvent != null )
                OnEPlayerStateChangedEvent(mPlayerState);
        }
    }

    /// <summary>
    /// What happens when EGameState change event is triggered
    /// </summary>
    /// <param name="value"></param>
    private void OnEPlayerStateChanged(EPlayerState value)
    {
        // state effects

        switch ( value )
        {
            case EPlayerState.STATE_IDLE:
            default:
                Debug.Log("Waiting for input from " + playerId.ToString());
                if ( IsInvoking("ChangePlayerStateToStateIdleFromInactivity") )
                    CancelInvoke("ChangePlayerStateToStateIdleFromInactivity");
                gameManager.DisplayPlayerStateUI(playerId, false);
                if ( gameManager.AreBothPlayersIdle() )
                {
                    gameManager.e_State = EGameState.STATE_IDLE;
                }
                break;

            case EPlayerState.STATE_FOUND:
                Debug.Log("Found " + playerId.ToString());
                Invoke("ChangePlayerStateToStateIdleFromInactivity", allowedIdleTimeAfterFindingPlayer);
                if (gameManager.e_State == EGameState.STATE_IDLE )
                {
                    gameManager.e_State = EGameState.STATE_MENU;
                }
                gameManager.ChangePlayerStateInfo(playerId, value);
                gameManager.DisplayPlayerStateUI(playerId, true);
                break;

            case EPlayerState.STATE_GAME_INACTIVE:
                Debug.Log(playerId.ToString() + " is not ready");
                break;

            case EPlayerState.STATE_READY:
                Debug.Log(playerId.ToString() + " is ready");
                if ( IsInvoking("ChangePlayerStateToStateIdleFromInactivity") )
                    CancelInvoke("ChangePlayerStateToStateIdleFromInactivity");
                gameManager.ChangePlayerStateInfo(playerId, value);
                break;
        }
    }

    /// <summary>
    /// Change state back to ready after returning to the players starting position
    /// </summary>
    private void ChangeBackToReadyAfterScore()
    {
        if ( playerState == EPlayerState.STATE_GAME_INACTIVE && transform.position.Equals(startingPosition) )
            playerState = EPlayerState.STATE_READY;
    }

    void ChangePlayerStateToStateIdleFromInactivity()
    {
        if ( playerState == EPlayerState.STATE_FOUND )
            playerState = EPlayerState.STATE_IDLE;
    }
}
