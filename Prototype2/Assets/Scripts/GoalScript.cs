﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalScript : MonoBehaviour {

    public int score;
    public bool hasScored;
    public EPlayerId playerId;

    [FMODUnity.EventRef]
    public string goalNoise = "event:/Goal";       //Create the eventref and define the event path

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ball")
        {
            if (hasScored)
            {
                score++;
            }
            hasScored = true;
            FMODUnity.RuntimeManager.GetBus("bus:/").stopAllEvents(FMOD.Studio.STOP_MODE.IMMEDIATE);
            FMODUnity.RuntimeManager.PlayOneShot(goalNoise);
            
        }
    }
}
