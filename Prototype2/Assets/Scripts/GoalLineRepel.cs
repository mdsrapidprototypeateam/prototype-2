﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalLineRepel : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<PlayerId>().playerId != EPlayerId.NONE || collision.transform.parent.GetComponent<BallBounce>().takenBy != EPlayerId.NONE)
        {
            Vector2 colDirection = transform.position - collision.transform.position;
            Vector2 colPoint = colDirection + (Vector2)collision.transform.position;
            Vector2 normalStarter = colPoint - (Vector2)collision.transform.position;
            Vector2 normal = new Vector2(-normalStarter.y, normalStarter.x);
            Vector2 speedVec = collision.GetComponent<Rigidbody2D>().velocity.normalized;
            Vector2 reflectDir = Vector2.Reflect(speedVec,  normal);
            collision.GetComponent<Rigidbody2D>().velocity = reflectDir * Mathf.Max(20, 30);
        }
    }
}
