﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpSpawner : MonoBehaviour
{

    public Sprite[] powerUpSprites;
    public GameObject powerUpBase;
    public float spawnPUPTimer;

    private MyGameManager gameManager;

    public bool spawnTimerActive;// variable that represents the need for spawning a powerup
    private float innerTimer;



    public GameObject goalPlayer1;
    public GameObject goalPlayer2;
    public Vector3 initialSizeGoals;

    public GameObject[] balls;
    public float originalBallSpeed;
	public Text announcer;

    [HideInInspector]
    public float goalTimers;
	[HideInInspector]
    public float ballTimers;
	//[HideInInspector]
	public float extraPucksTimers;


	public GameObject puckPrefab;
	public int spawnPuckRadius;




    // Use this for initialization
    void Start()
    {
        if (!gameManager)
        {
            gameManager = (MyGameManager)GameObject.Find("GameManager").GetComponent<MyGameManager>();
        }

        goalPlayer1 = GameObject.Find("GoalPlayer1");
        goalPlayer2 = GameObject.Find("GoalPlayer2");
        initialSizeGoals = goalPlayer1.transform.localScale;
        goalTimers = 0;

        balls = new GameObject[100];

        balls[0] = GameObject.Find("Ball");

        originalBallSpeed = balls[0].GetComponent<BallRotation>().rotationSpeed;
        ballTimers = 0;

    }

    // Update is called once per frame
    void Update()
    {
        if(gameManager.e_State==EGameState.STATE_COUNTDOWN)
        {
            spawnTimerActive = true;
        }else if(gameManager.e_State == EGameState.STATE_FINISH || gameManager.e_State ==EGameState.STATE_GAME_SCORED|| gameManager.e_State == EGameState.STATE_BALL_OUTOFFBOUNDS)
        {
            CleanAllPowerUps();
        }

        if (spawnTimerActive)
        {
            SpawnTimer();
        }


        //powerUp resets
        CleanGoalPowerUps();
        CleanBallPowerUps();
		ClearExtraPucks ();
    }


    //function responsable for spawing a powerup after a certain time when ever it is necessary
    void SpawnTimer()
    {
        innerTimer += Time.deltaTime;

        if (innerTimer > spawnPUPTimer)
        {
            GameObject pUp = Instantiate(powerUpBase, transform.position, Quaternion.identity, this.transform);
            int type = RandomPowerUp();
            Debug.Log(type);
            pUp.GetComponent<PowerUpBase>().type = type;
            pUp.GetComponent<SpriteRenderer>().sprite = powerUpSprites[type];
            pUp.GetComponent<PowerUpBase>().spawnerBase = this;
			pUp.GetComponent<PowerUpBase> ().announcer = announcer;
            spawnTimerActive = false;
            innerTimer = 0;
        }

    }


    //responsable for picking a random power up to spawn
    // this function can be afjusted in order to have some powerups have more possibility of spawn then others
    private int RandomPowerUp()
    {
        float ranNum = Random.Range(0, 100);

        if(ranNum<=20)
        {
            return 0;
        }
        else if(ranNum>20 && ranNum<=40)
        {
            return 1;
        }
        else if (ranNum > 40 && ranNum <= 60)
        {
            return 2;
        }
		else if (ranNum > 60 && ranNum<= 80)
        {
            return 3;
        }
		else if (ranNum > 80)
		{
			return 4;
		}
        else
        {
            //something went wrong
            return 0;
        }
       
    }



    //function responsable for after a certain time restoring the goal to its original size
    void CleanGoalPowerUps()
    {
        if(goalTimers >0)
        {
            goalTimers -= Time.deltaTime;
        }
        else if( goalTimers < 0 )
        {
            Debug.Log("trigger");
            goalTimers = 0;
            goalPlayer1.transform.localScale = initialSizeGoals;
            goalPlayer2.transform.localScale = initialSizeGoals;
        }

    }

	//function responsable for restoring the ball to istn normal speed afte4r a few seconds
    void CleanBallPowerUps()
    {
        if (ballTimers > 0)
        {
            ballTimers -= Time.deltaTime;
        }
        else if (ballTimers < 0)
        {
            ballTimers = 0;
            for(int i = 0; i <balls.Length;i++)
            {
                if (balls[i])
                {
                    balls[i].GetComponent<BallRotation>().rotationSpeed = originalBallSpeed;
                }
            }
            
        }

    }

	//Function responsable for clearing extra balls of the game after a few seconds

	public void ClearExtraPucks()
	{
		if (extraPucksTimers > 0)
		{
			//Debug.Log ("time is ticking");
			extraPucksTimers -= Time.deltaTime;
		}
		else if (extraPucksTimers < 0)
		{
			//Debug.Log ("i got here");
			for(int i = 1; i <balls.Length;i++)
			{
				if (balls[i])
				{
					Destroy (balls [i].gameObject);
				}
			}

			//Debug.Log ("i got here 2");
			extraPucksTimers = 0;

		}

	}


	public void RemoteResetAnnouncer()
	{
		Invoke ("ResetAnnouncer", 2f);
	}

	void ResetAnnouncer()
	{
		announcer.text = "";
	}



    void CleanAllPowerUps()
    {
        //goals
        goalTimers = 0;
        goalPlayer1.transform.localScale = initialSizeGoals;
        goalPlayer2.transform.localScale = initialSizeGoals;


        //balls
        ballTimers = 0;
        for (int i = 0; i < balls.Length; i++)
        {
            if (balls[i])
            {
                balls[i].GetComponent<BallRotation>().rotationSpeed = originalBallSpeed;
            }
        }

        //extra pucks
        extraPucksTimers = 0;
        for (int i = 1; i < balls.Length; i++)
        {
            if (balls[i])
            {
                Destroy(balls[i].gameObject);
            }
        }
    }



    void OnTriggerEnter2D(Collider2D col)
    {

    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("powerUp"))
        {
            spawnTimerActive = true;
        }

    }
}
