﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpBase : MonoBehaviour
{

    public float[] deathCD;
    private float innerTimer;
    public int type;
    public float rotSpeed;
	public Text announcer;


    public PowerUpSpawner spawnerBase;
    public int fastBallSpeed;
    public int slowBallSpeed;
    public int ballTimer;
	public int extraPuckTimer;

    public int goalTimer;

    [FMODUnity.EventRef]
    public string powerUp = "event:/PowerUp";       //Create the eventref and define the event path


    private bool active;
    private int owner; // this refers to the player that has triggered the power up

    // Use this for initialization
    void Start()
    {
        active = false;
     
    }

    // Update is called once per frame
    void Update()
    {
        DeathTimer();
        PowerUpEffect();
        DoABarrelRoll();
    }

    void DeathTimer()
    {
        innerTimer += Time.deltaTime;

        if (innerTimer > deathCD[type])
        {
            Destroy(this.gameObject);

        }
    }

    void PowerUpEffect()
    {
        if (active)
        {
            switch (type)
            {
			case 0:
				announcer.text = "SMALL GOALS";
				spawnerBase.RemoteResetAnnouncer ();
                    SmallGoalPowerUp();
                    Destroy(this.gameObject);
                    break;
                case 1:
					announcer.text = "BIG GOALS";
				spawnerBase.RemoteResetAnnouncer ();
                    BigGoalPowerUp();
                    Destroy(this.gameObject);
                    break;
                case 2:
					announcer.text = "FASTER PUCK SPIN";
				spawnerBase.RemoteResetAnnouncer ();
                    FasterBallPowerUp();
                    Destroy(this.gameObject);
                    break;
			case 3:
				announcer.text = "SLOW PUCK SPIN";
				SlowBallPowerUp ();
				spawnerBase.RemoteResetAnnouncer ();
                    Destroy(this.gameObject);

                    break;
			case 4:
				announcer.text = "RAINING PUCKS";
				ManyBallsPowerUp ();
				spawnerBase.RemoteResetAnnouncer ();
				Destroy(this.gameObject);
				break;
			
            }
        }

    }

    //just makes the object spin for visual effects
    void DoABarrelRoll()
    {
        Vector3 spinRot = new Vector3(0, 0, 1);
        transform.Rotate(spinRot * Time.deltaTime * rotSpeed);

    }




    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {

            if (col.GetComponent<PlayerId>().playerId == EPlayerId.PLAYER1)
            {
                owner = 1;
            }
            else
            {
                owner = 2;
            }

            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<CircleCollider2D>().enabled = false;
            active = true;
            FMODUnity.RuntimeManager.PlayOneShot(powerUp);

        }

    }


    void SmallGoalPowerUp()
    {
        
        Vector3 newSize = spawnerBase.initialSizeGoals * 0.75f;
        spawnerBase.goalPlayer1.transform.localScale = newSize;
        spawnerBase.goalPlayer2.transform.localScale = newSize;

        spawnerBase.goalTimers = goalTimer;

    }
    void BigGoalPowerUp()
    {
        Vector3 newSize = spawnerBase.initialSizeGoals * 1.5f;
        spawnerBase.goalPlayer1.transform.localScale = newSize;
        spawnerBase.goalPlayer2.transform.localScale = newSize;

        spawnerBase.goalTimers = goalTimer;

    }



    void FasterBallPowerUp()
    {
        for (int i = 0; i < spawnerBase.balls.Length; i++)
        {
            if (spawnerBase.balls[i])
            {
                spawnerBase.balls[i].GetComponent<BallRotation>().rotationSpeed = fastBallSpeed;
            }
        }
        spawnerBase.ballTimers = ballTimer;
    }
    void SlowBallPowerUp()
    {
        for (int i = 0; i < spawnerBase.balls.Length; i++)
        {
            if(spawnerBase.balls[i])
            {
                spawnerBase.balls[i].GetComponent<BallRotation>().rotationSpeed = slowBallSpeed;
            }
            
        }

        spawnerBase.ballTimers = ballTimer;

    }


    void StickyWallsPowerUp()
    {

    }


	//Spawn a random quantity of balls around a radius of the center of the map
    void ManyBallsPowerUp()
    {
		int numofPucks = Mathf.RoundToInt(Random.Range(6,13));
		int j = 0;
		for (int i = 0; i < spawnerBase.balls.Length; i++)
		{
			if(spawnerBase.balls[i])
			{
				j = i;
			}

		}


		for(int i = 0; i<numofPucks;i++)
			{
				float randomXPosDev = Random.Range (-spawnerBase.spawnPuckRadius, spawnerBase.spawnPuckRadius);
				float randomyPosDev = Random.Range (-spawnerBase.spawnPuckRadius, spawnerBase.spawnPuckRadius);
				Vector3 finalPos = new Vector3 (randomXPosDev, randomyPosDev, 0);
				GameObject pUp = Instantiate(spawnerBase.puckPrefab, finalPos, Quaternion.identity);
				spawnerBase.balls [j + 1 + i] = pUp;

			}


		spawnerBase.extraPucksTimers = extraPuckTimer;

    }



    void FastMovPowerUp()
    {

    }
    void SlowMovPowerUp()
    {

    }


}
