﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisionWithWall : MonoBehaviour {

    bool isOnArea;
    PlayerGetBall ball;

    public int type;

	// Use this for initialization
	void Start () {
        ball = GetComponent<PlayerGetBall>();
	}
	
	// Update is called once per frame
	void Update () {
        if (isOnArea)
        {
            switch (type)
            {
                case 1:
                    if (ball.ball.transform.position.y >= transform.position.y + 0.5f)
                    {
                        BallRotation newBall = ball.ball;
                        newBall.rb.isKinematic = false;
                        newBall.rb.AddForce(ball.transform.up * 500);
                        newBall.shouldRotate = false;
                        newBall.transform.parent = null;
                        GetComponent<PlayerGetBall>().ball = null;
                        newBall.GetComponent<BallBounce>().takenBy = EPlayerId.NONE;
                    }
                    break;
                case 2:
                    if (ball.ball.transform.position.y <= transform.position.y - 0.5f)
                    {
                        BallRotation newBall = ball.ball;
                        newBall.rb.isKinematic = false;
                        newBall.rb.AddForce(-ball.transform.up * 500);
                        newBall.shouldRotate = false;
                        newBall.transform.parent = null;
                        GetComponent<PlayerGetBall>().ball = null;
                        newBall.GetComponent<BallBounce>().takenBy = EPlayerId.NONE;
                    }
                    break;
                case 3:
                    if (ball.ball.transform.position.x >= transform.position.x)
                    {
                        BallRotation newBall = ball.ball;
                        newBall.rb.isKinematic = false;
                        newBall.rb.AddForce(- new Vector2(0.5f, 0.5f) * 500);
                        newBall.shouldRotate = false;
                        newBall.transform.parent = null;
                        GetComponent<PlayerGetBall>().ball = null;
                        newBall.GetComponent<BallBounce>().takenBy = EPlayerId.NONE;
                    }
                    break;
                case 4:
                    if (ball.ball.transform.position.x <= transform.position.x)
                    {
                        BallRotation newBall = ball.ball;
                        newBall.rb.isKinematic = false;
                        newBall.rb.AddForce( new Vector2(0.5f, 0.5f) * 500);
                        newBall.shouldRotate = false;
                        newBall.transform.parent = null;
                        GetComponent<PlayerGetBall>().ball = null;
                        newBall.GetComponent<BallBounce>().takenBy = EPlayerId.NONE;
                    }
                    break;
            }
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "ballSender")
        {
            isOnArea = true;
            type = collision.GetComponent<WallType>().type;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "ballSender")
        {
            isOnArea = false;
        }
    }
}
