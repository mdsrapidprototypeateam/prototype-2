﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : PlayerInputBase
{
    Rigidbody2D rb;
    public float movementSpeed;
    public bool tangent = true;

    [Header("Dash settings")]
    public float dashSpeed;
    public float dashCooldown;
    public float dashGrace;
    public bool canCatch;
    bool canDash = true;

    [Space(6f)]
    [Header("Movement")]
    public float yRotation = 0f;
    public float yRotationSpeed = 10f;
    public float characterSpeed = 10f;

    [FMODUnity.EventRef]
    public string shootSound = "event:/Shoot";       //Create the eventref and define the event path
    public string dashSound = "event:/Dash";

    public override void Start ()
    {
        base.Start();
        rb = GetComponent<Rigidbody2D>();
    }

    public override void Update ()
    {
        GetPlayerInput(Time.deltaTime);
	}

    /// <summary>
    /// Collect input from current user
    /// </summary>
    public override void GetPlayerInput(float deltaTime)
    {
        if (gameManager.e_State == EGameState.STATE_GAME_STARTED)
        {
            m_fHorizontalInput = Input.GetAxis(horizontalAxisName);
            if ( m_fHorizontalInput > 0 )
            {
                rb.AddForce(new Vector2(movementSpeed * deltaTime, 0));
            }
            else if ( m_fHorizontalInput < 0 )
            {
                rb.AddForce(new Vector2(-movementSpeed * deltaTime, 0));
            }
            m_fVerticalInput = Input.GetAxis(verticalAxisName);
            if ( m_fVerticalInput > 0 )
            {
                rb.AddForce(new Vector2(0, movementSpeed * deltaTime));
            }
            else if ( m_fVerticalInput < 0 )
            {
                rb.AddForce(new Vector2(0, -movementSpeed * deltaTime));
            }

            if ( Input.GetAxis(fireButtonName) > 0 && !m_bIsFireHold )
            {
                BallRotation ball = GetComponent<PlayerGetBall>().ball;
                m_bIsFireHold = true;

                if (ball)
                {
                    ball.rb.isKinematic = false;

                    if (tangent)
                    {
                        ball.rb.AddForce(ball.transform.up * 500);
                    }
                    else
                    {
                        ball.rb.AddForce(ball.transform.right * 500);
                    }
                    ball.shouldRotate = false;
                    ball.transform.parent = null;
                    GetComponent<PlayerGetBall>().ball = null;
                    ball.GetComponent<BallBounce>().takenBy = EPlayerId.NONE;
                    ball.GetComponentInChildren<Collider2D>().gameObject.layer = 8;
                    FMODUnity.RuntimeManager.PlayOneShot(shootSound);
                }
            }
            else if ( Input.GetAxis(fireButtonName) == 0 && m_bIsFireHold )
                m_bIsFireHold = false;

            if ( Input.GetAxis(jumpButtonName) > 0 && !m_bIsJumpHold && canDash )
            {
                m_bIsJumpHold = true;
                rb.AddForce(new Vector2(m_fHorizontalInput, m_fVerticalInput).normalized * dashSpeed);
                canDash = false;
                canCatch = true;
                FMODUnity.RuntimeManager.PlayOneShot(dashSound);
                StartCoroutine(DashCooldown());
                StartCoroutine(DashGracePeriod());
            }
            else if ( Input.GetAxis(jumpButtonName) == 0 && m_bIsJumpHold )
                m_bIsJumpHold = false;

            if ( Input.GetAxis(startButtonName) > 0 && !m_bIsStartHold )
                m_bIsStartHold = true;
            else if ( Input.GetAxis(startButtonName) == 0 && m_bIsStartHold )
                m_bIsStartHold = false;
        }
    }

    IEnumerator DashCooldown()
    {
        yield return new WaitForSeconds(dashCooldown);
        canDash = true;
    }

    IEnumerator DashGracePeriod()
    {
        yield return new WaitForSeconds(dashGrace);
        canCatch = false;
    }

}
