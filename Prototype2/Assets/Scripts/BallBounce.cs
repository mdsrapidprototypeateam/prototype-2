﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBounce : MonoBehaviour {

    Vector2 previousVelocity;
    public Rigidbody2D rb;
    public float bounciness;
    public EPlayerId takenBy = EPlayerId.NONE;
    public bool isOnWall;
    public float currentTime;
    public float maxTime;
    int type;
	public GameObject shockwave;

    [FMODUnity.EventRef]
    public string bounce = "event:/Bounce";       //Create the eventref and define the event path
    FMOD.Studio.EventInstance bounceEvent;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        previousVelocity = rb.velocity;

	}

    void Reflect(Vector2 normal)
    {
        float speed = previousVelocity.magnitude;
        Vector2 reflectDir = Vector2.Reflect(previousVelocity.normalized, normal);
        rb.velocity = reflectDir * Mathf.Max(speed, bounciness);
        FMODUnity.RuntimeManager.PlayOneShot(bounce);
		Instantiate (shockwave, transform.position, Quaternion.identity, null);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag != "Ball")
        {
            transform.parent = null;

            /*if (rb.velocity == Vector2.zero)
            {
                rb.AddForce(-transform.up * 500);

            }*/

			if (takenBy != EPlayerId.NONE)
			{
				rb.AddForce(-transform.up * 500);

			}

            if (collision.transform.tag != "player")
            {
                if (takenBy == EPlayerId.PLAYER1)
                {
                    GameObject.Find("Player1").GetComponent<PlayerGetBall>().ball = null;
                }
                else if (takenBy == EPlayerId.PLAYER2)
                {
                    GameObject.Find("Player2").GetComponent<PlayerGetBall>().ball = null;
                }
                GetComponent<BallRotation>().shouldRotate = false;
            }


            if (collision.contacts.Length > 0)
                Reflect(collision.contacts[0].normal);

            /* if (takenBy != EPlayerId.NONE)
             {
                 rb.AddForce(Vector2.Reflect(previousVelocity.normalized, collision.contacts[0].normal) * 500, ForceMode2D.Impulse);
             }*/

            GetComponentInChildren<Collider2D>().gameObject.layer = 8;
            takenBy = EPlayerId.NONE;
        }



    }

   private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.tag == "ballWall" || collision.tag == "goalArea") && takenBy != EPlayerId.NONE )
        {
			rb.isKinematic = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
		if ((collision.tag == "ballWall" || collision.tag == "goalArea") && takenBy != EPlayerId.NONE)
        {
			rb.isKinematic = true;
        }
    }
}
