﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallRotation : MonoBehaviour {

   // public bool shouldRotate;
    public float rotationSpeed;
    public Rigidbody2D rb;
    public Vector2 velocity;
	public float spinRadius;
    public bool startSound;

	public GameObject trail;

    private bool _shouldRotate = false;

    public bool shouldRotate
    {
        get { return _shouldRotate; }
        set
        {
            if (_shouldRotate != value)
            {
                _shouldRotate = value;

                if (_shouldRotate)
                {
                    rotationEvent = FMODUnity.RuntimeManager.CreateInstance(rotation);
                    rotationEvent.start();
					trail.SetActive (false);
                } else
                {
                    rotationEvent.release();
                    rotationEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
					trail.SetActive (true);
                }
                // Run some function or event here
            }
        }
    }

    [FMODUnity.EventRef]
    public string rotation = "event:/Rotation";       //Create the eventref and define the event path
    FMOD.Studio.EventInstance rotationEvent;


    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        velocity = rb.velocity;
        if (_shouldRotate)
        {
            rotationEvent.setParameterValue("Speed", rotationSpeed);
            //shouldStartRunning = false;
            //rb.angularVelocity = rotationSpeed;
            transform.RotateAround(transform.parent.position, Vector3.forward, rotationSpeed);
            //rb.velocity = transform.localPosition;
            //transform.Rotate(Vector3.forward, rotationSpeed);
        }
	}
}
