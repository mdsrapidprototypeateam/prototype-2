﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum EGameState
{
    NONE,
    STATE_IDLE,
    STATE_MENU,
    STATE_COUNTDOWN,
    STATE_GAME_STARTED,
    STATE_GAME_SCORED,
    STATE_BALL_OUTOFFBOUNDS,
    STATE_FINISH
    //STATE_WIN
}

public class MyGameManager : MonoBehaviour
{
    public EGameState e_State = EGameState.NONE;
    private EGameState m_EState;

    public PlayerId player1IdComp;
    public PlayerId player2IdComp;

    private GameObject[] playerGameObjects;
    public GameObject player1GameObj;
    public GameObject player2GameObj;
    public GoalScript player1TargetGoal;
    public GoalScript player2TargetGoal;
    public GameObject ballGameObject;
    int countPlayerGameObjects;
    private Vector3 ballStartingPosition;
    public GameObject[] powerUpBaseGroup;

	public ParticleSystem particleSystem;

    public delegate void EGameStateChanged(EGameState value);
    public event EGameStateChanged OnEGameStateChangedEvent;

    [Header("Time intervals")]
    public float timeSecondsAfterScore = 3.0f;
    public float countdownIncrementDeltaTime = 1.0f;
    public int countdownStart = 3;
    public int finishCountdownStart = 13;
    public int currentCountdown;
    public int maxGameTimeSeconds = 60;
    public int currentGameTimeSeconds;

    [Header("Menu fields")]
    public GameObject gameTitleField;
    public GameObject idleInfoField;
    public GameObject player1StateField;
    public GameObject player2StateField;
    public GameObject timerField;
    public GameObject countdownField;
    public GameObject player1ScoreField;
    public GameObject player2ScoreField;
    public GameObject finishCountdownField;
    public GameObject powerUpInfoField;

    public Text idleInfoTextComponent;
    public Text countdownTextComponent;
    public Text finishCountdownTextComponent;
    public Text timerTextComponent;
    public Text player1StateTextComponent;
    public Text player2StateTextComponent;
    public Text player1ScoreTextComponent;
    public Text player2ScoreTextComponent;
    public Text powerUpInfoTextComponent;

    private bool didGameStartNow = false;

    private Camera mainCamera;

    bool refereePlayed;

    [FMODUnity.EventRef]
    public string referee = "event:/Referee";
    public string countSound = "event:/Count"; //Create the eventref and define the event path
    public string song = "event:/Song";

    void Start()
    {
        //Find UI gameObjects and text fields
        FindUI();

        //Find players
        FindPlayerGameObjects();

        //Find ball and it's starting position
        FindBallGameObject();

        //Find each player target goal
        FindPlayerTargetGoals();

        //Start tracking changes to game state
        StartEGameStateCounter();
        GetEGameState();

        //Set initial game state to idle
        if ( !e_State.Equals(EGameState.STATE_IDLE) )
            e_State = EGameState.STATE_IDLE;

        mainCamera = Camera.main;
    }


    void Update()
    {
        //Get current game state
        GetEGameState();

        //Currently on menu game state; find if both players are ready
        if ( AreBothPlayersReady(player1IdComp, player2IdComp) && e_State == EGameState.STATE_MENU )
        {
            didGameStartNow = true;
            Invoke("ChangeGameStateToCountdown", 1.0f);
        }

        //Gameplay logic
        if ( e_State == EGameState.STATE_GAME_STARTED )
        {
            //Check for goals
            if ( player1TargetGoal.hasScored || player2TargetGoal.hasScored )
            {
                if ( player1TargetGoal.hasScored )
                    player1TargetGoal.hasScored = false;
                else
                    player2TargetGoal.hasScored = false;

                e_State = EGameState.STATE_GAME_SCORED;
            }

            //check if ball is out bounds
            CheckIfBallIsOutOfBounds();
        }
    }

    /// <summary>
    /// Finds each player GameObject and their PlayerId component based on their id
    /// </summary>
    void FindPlayerGameObjects()
    {
        if ( player1IdComp == null || player2IdComp == null )
        {
            playerGameObjects = GameObject.FindGameObjectsWithTag("Player");
            countPlayerGameObjects = playerGameObjects.Length;
            for ( int i = 0; i < countPlayerGameObjects; i++ )
            {
                GameObject currentPlayerGameObj = playerGameObjects[i];
                PlayerId currentObjPlayerIdComponent = currentPlayerGameObj.GetComponent<PlayerId>();
                if ( currentObjPlayerIdComponent.playerId == EPlayerId.PLAYER1 )
                {
                    player1IdComp = currentObjPlayerIdComponent;
                    player1GameObj = currentPlayerGameObj;
                }
                else if ( currentObjPlayerIdComponent.playerId == EPlayerId.PLAYER2 )
                {
                    player2IdComp = currentObjPlayerIdComponent;
                    player2GameObj = currentPlayerGameObj;
                }
            }
        }
    }

    /// <summary>
    /// Finds each player target goal script
    /// </summary>
    void FindPlayerTargetGoals()
    {
        if ( player1TargetGoal == null || player2TargetGoal == null )
        {
            GameObject playerTargetGoalgameObj = GameObject.Find("GoalPlayer2");
            if ( !playerTargetGoalgameObj )
                Debug.LogError("Player1 target goal name must be \"GoalPlayer2\"");
            else player1TargetGoal = playerTargetGoalgameObj.GetComponent<GoalScript>();

            playerTargetGoalgameObj = GameObject.Find("GoalPlayer1");
            if ( !playerTargetGoalgameObj )
                Debug.LogError("Player2 target goal name must be \"GoalPlayer1\"");
            else player2TargetGoal = playerTargetGoalgameObj.GetComponent<GoalScript>();
        }
    }

    /// <summary>
    /// Finds ball game object and its starting position
    /// </summary>
    void FindBallGameObject()
    {
        ballGameObject = GameObject.Find("Ball");
        ballStartingPosition = ballGameObject.transform.position;
    }

    /// <summary>
    /// Auxiliary counter for tracking changes on EGameState property
    /// </summary>
    private void StartEGameStateCounter()
    {
        OnEGameStateChangedEvent += OnEGameStateChanged;
    }

    /// <summary>
    /// Get current EGameState public and private property values.
    /// Triggers event if they are not the same.
    /// /// </summary>
    public void GetEGameState()
    {
        if ( e_State != m_EState )
        {
            m_EState = e_State;

            if ( OnEGameStateChangedEvent != null )
                OnEGameStateChangedEvent(m_EState);
        }
    }

    /// <summary>
    /// What happens when EGameState change event is triggered
    /// </summary>
    /// <param name="value"></param>
    private void OnEGameStateChanged(EGameState value)
    {
        // state effects

        switch ( value )
        {
            case EGameState.STATE_IDLE:
            default:
                Debug.Log("Waiting player input for start");
                if ( IsInvoking("ChangeGameStateToIdle") )
                    CancelInvoke("ChangeGameStateToIdle");
                
                idleInfoTextComponent.text = "PRESS START";
                gameTitleField.SetActive(true);
                idleInfoField.SetActive(true);
                player1StateField.SetActive(false);
                player2StateField.SetActive(false);
                timerField.SetActive(false);
                countdownField.SetActive(false);
                player1ScoreField.SetActive(false);
                player2ScoreField.SetActive(false);
                finishCountdownField.SetActive(false);
                powerUpInfoField.SetActive(false);
                powerUpInfoTextComponent.text = "";
                break;

            case EGameState.STATE_MENU:
                Debug.Log("Game displaying menu");
                if(IsInvoking("DecreaseFinishCountdown") )
                    CancelInvoke("DecreaseFinishCountdown");
                Invoke("ChangeGameStateToIdle", player1IdComp.allowedIdleTimeAfterFindingPlayer);

                player1TargetGoal.score = 0;
                player2TargetGoal.score = 0;
                UpdateScores();

                //reset each player position
                player1GameObj.transform.position = player1IdComp.startingPosition;
                player2GameObj.transform.position = player2IdComp.startingPosition;

                //reset ball position
                ballGameObject.transform.position = ballStartingPosition;

                gameTitleField.SetActive(true);
                idleInfoField.SetActive(false);
                timerField.SetActive(false);
                countdownField.SetActive(false);
                player1ScoreField.SetActive(false);
                player2ScoreField.SetActive(false);
                finishCountdownField.SetActive(false);
                powerUpInfoField.SetActive(false);
                currentGameTimeSeconds = maxGameTimeSeconds;
                currentCountdown = countdownStart;
                break;

            case EGameState.STATE_COUNTDOWN:
                Debug.Log("Start Countdown");
                FMODUnity.RuntimeManager.PlayOneShot(countSound);
                if ( IsInvoking("ChangeGameStateToIdle") )
                    CancelInvoke("ChangeGameStateToIdle");
                countdownTextComponent.text = currentCountdown.ToString();

                idleInfoField.SetActive(false);
                gameTitleField.SetActive(false);
                countdownField.SetActive(true);
                player1StateField.SetActive(false);
                player2StateField.SetActive(false);
                InvokeRepeating("DecreaseCountdown", 1.0f, countdownIncrementDeltaTime);
                break;

		case EGameState.STATE_GAME_STARTED:
			particleSystem.gameObject.SetActive (false);
                Debug.Log("Game started");
                if (IsInvoking("DecreaseCountdown") )
                    CancelInvoke("DecreaseCountdown");
                countdownField.SetActive(false);
                if(didGameStartNow)
                {
                    didGameStartNow = false;
                    timerField.SetActive(true);
                    player1ScoreField.SetActive(true);
                    player2ScoreField.SetActive(true);
                    powerUpInfoField.SetActive(true);
                    InvokeRepeating("DecrementTime", 0.0f, 1.0f);
                }
                break;

            case EGameState.STATE_BALL_OUTOFFBOUNDS:
            case EGameState.STATE_GAME_SCORED:
                Debug.Log("Game scored");
                currentCountdown = countdownStart;
                UpdateScores();
                ReleaseBall();
                CleanUpExtraBalls();
                player1IdComp.playerState = EPlayerState.STATE_GAME_INACTIVE;
                player2IdComp.playerState = EPlayerState.STATE_GAME_INACTIVE;
                ballGameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                Invoke("ResetPlayerAndBallPositions", timeSecondsAfterScore);

			if(value.Equals(EGameState.STATE_GAME_SCORED)){
                    idleInfoTextComponent.text = "GOAL!!!";
				particleSystem.gameObject.SetActive (true);
			}
                else
                    idleInfoTextComponent.text = "OUT OF BOUNDS!!!";
                idleInfoField.SetActive(true);

                break;

            case EGameState.STATE_FINISH:
                Debug.Log("Current Match ended");
                if(IsInvoking("DecrementTime") )
                    CancelInvoke("DecrementTime");

                finishCountdownField.SetActive(true);
                CleanUpExtraBalls();

                //ballGameObject.GetComponent<BallRotation>().shouldRotate = false;
                ReleaseBall();
                ballGameObject.GetComponent<Rigidbody2D>().isKinematic = true;
                ballGameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

                player1IdComp.playerState = EPlayerState.STATE_GAME_INACTIVE;
                player2IdComp.playerState = EPlayerState.STATE_GAME_INACTIVE;

                currentCountdown = finishCountdownStart;

                string finishPhrase = "CONGRATULATIONS ";
                if ( player1TargetGoal.score > player2TargetGoal.score )
                    finishPhrase += "PLAYER 1\nYOU WON!";
                else if ( player1TargetGoal.score < player2TargetGoal.score )
                    finishPhrase += "PLAYER 2\nYOU WON!";
                else
                    finishPhrase = "IT'S A DRAW";

                finishPhrase += "\nREPLAY?\npress start";

                idleInfoTextComponent.text = finishPhrase;
                InvokeRepeating("DecreaseFinishCountdown", countdownIncrementDeltaTime, countdownIncrementDeltaTime);
                break;
        }
    }

    /// <summary>
    /// Get both player isReady value
    /// </summary>
    /// <param name="player1">The playerId component for player 1</param>
    /// <param name="player2">The playerId component for player 2</param>
    /// <returns></returns>
    bool AreBothPlayersReady(PlayerId player1, PlayerId player2)
    {
        if ( player1.playerState == EPlayerState.STATE_READY && player2.playerState == EPlayerState.STATE_READY )
            return true;
        else return false;
    }

    void ChangeGameStateToCountdown()
    {
        if ( e_State == EGameState.STATE_MENU || e_State == EGameState.STATE_GAME_SCORED || e_State == EGameState.STATE_BALL_OUTOFFBOUNDS )
        {
            e_State = EGameState.STATE_COUNTDOWN;
        }
    }

    void ResetPlayerAndBallPositions()
    {
        //reset each player position
        player1GameObj.transform.position = player1IdComp.startingPosition;
        player2GameObj.transform.position = player2IdComp.startingPosition;

        player1IdComp.playerState = EPlayerState.STATE_READY;
        player2IdComp.playerState = EPlayerState.STATE_READY;

        //reset ball position
        ballGameObject.transform.position = ballStartingPosition;

        //Change to countdown after 1 second
        Invoke("ChangeGameStateToCountdown", 1.0f);
    }

    /// <summary>
    /// Initialize UI related properties
    /// </summary>
    void FindUI()
    {
        if ( !gameTitleField )
            gameTitleField = GameObject.Find("GameTitle");

        if ( !idleInfoField )
        {
            idleInfoField = GameObject.Find("IdleStateInfo");
            idleInfoTextComponent = idleInfoField.GetComponent<Text>();
        }

        if ( !player1StateField )
        {
            player1StateField = GameObject.Find("Player1State");
            player1StateTextComponent = player1StateField.GetComponent<Text>();
        }

        if ( !player2StateField )
        {
            player2StateField = GameObject.Find("Player2State");
            player2StateTextComponent = player2StateField.GetComponent<Text>();
        }

        if ( !timerField )
        {
            timerField = GameObject.Find("Timer");
            timerTextComponent = timerField.GetComponent<Text>();
        }

        if ( !countdownField )
        {
            countdownField = GameObject.Find("CountdownTimer");
            countdownTextComponent = countdownField.GetComponent<Text>();
        }

        if ( !player1ScoreField )
        {
            player1ScoreField = GameObject.Find("Player1Score");
            player1ScoreTextComponent = player1ScoreField.GetComponent<Text>();
        }

        if ( !player2ScoreField )
        {
            player2ScoreField = GameObject.Find("Player2Score");
            player2ScoreTextComponent = player2ScoreField.GetComponent<Text>();
        }

        if ( !finishCountdownField )
        {
            finishCountdownField = GameObject.Find("FinishCountdown");
            finishCountdownTextComponent = finishCountdownField.GetComponent<Text>();
        }

        if (!powerUpInfoField)
        {
            powerUpInfoField = GameObject.Find("PowerUpInfo");
            powerUpInfoTextComponent = powerUpInfoField.GetComponent<Text>();
        }
    }

    public bool AreBothPlayersIdle()
    {
        if ( player1IdComp.playerState == EPlayerState.STATE_IDLE && player2IdComp.playerState == EPlayerState.STATE_IDLE )
            return true;
        else return false;
    }

    public void DisplayPlayerStateUI(EPlayerId id, bool shouldDisplay)
    {
        if ( id.Equals(EPlayerId.PLAYER1) )
            player1StateField.SetActive(shouldDisplay);

        else if ( id.Equals(EPlayerId.PLAYER2) )
            player2StateField.SetActive(shouldDisplay);
    }

    public void ChangePlayerStateInfo(EPlayerId id, EPlayerState playerState)
    {
        if ( id == EPlayerId.PLAYER1 )
        {
            if ( playerState == EPlayerState.STATE_FOUND )
            {
                player1StateTextComponent.text = "PRESS START\nTO CONFIRM";
            }
            if ( playerState == EPlayerState.STATE_READY )
            {
                player1StateTextComponent.text = "PLAYER1\nREADY";
            }
        }

        else if ( id == EPlayerId.PLAYER2 )
        {
            if ( playerState == EPlayerState.STATE_FOUND )
            {
                player2StateTextComponent.text = "PRESS START\nTO CONFIRM";
            }
            if ( playerState == EPlayerState.STATE_READY )
            {
                player2StateTextComponent.text = "PLAYER2\nREADY";
            }
        }
    }

    private void DecreaseCountdown()
    {
        if ( currentCountdown == 0 )
        {
            e_State = EGameState.STATE_GAME_STARTED;
            FMODUnity.RuntimeManager.PlayOneShot(song);
            //CancelInvoke("DecreaseCountdown");
        }
        if (currentCountdown > 0)
        {
            --currentCountdown;
            FMODUnity.RuntimeManager.PlayOneShot(countSound);
        }

        if ( currentCountdown == 0 )
        {
            timerTextComponent.text = FormatTime(currentGameTimeSeconds);
            timerField.SetActive(true);
            countdownTextComponent.text = "GO";
            if (!refereePlayed) {
                FMODUnity.RuntimeManager.PlayOneShot(referee);
                refereePlayed = true;
            }

            else
            {
                refereePlayed = false;
            }
        }
        else countdownTextComponent.text = currentCountdown.ToString();
    }

    private void DecrementTime()
    {
        if ( currentGameTimeSeconds == 0 )
        {
            e_State = EGameState.STATE_FINISH;
            return;
            //CancelInvoke("DecrementTime");
        }

        if ( currentGameTimeSeconds > 0 )
            currentGameTimeSeconds -= 1;

        timerTextComponent.text = FormatTime(currentGameTimeSeconds);

        if ( currentGameTimeSeconds == 0 )
        {
            currentCountdown = finishCountdownStart;
            finishCountdownTextComponent.text = "\n\n\n\n" + currentCountdown.ToString();
            idleInfoTextComponent.text = "TIME'S UP";
			FMODUnity.RuntimeManager.PlayOneShot (referee);
            idleInfoField.SetActive(true);
        }
    }

    string FormatTime(int time)
    {
        int minutes = time / 60;
        int seconds = time % 60;
        string timeText = String.Format ("{0:00}:{1:00}", minutes, seconds);
        return timeText;
    }

    void UpdateScores()
    {
        string player1Score = String.Format ("PLAYER1\n{0:00000}", player1TargetGoal.score * 10);
        string player2Score = String.Format ("PLAYER2\n{0:00000}", player2TargetGoal.score * 10);
        player1ScoreTextComponent.text = player1Score;
        player2ScoreTextComponent.text = player2Score;
    }

    void DecreaseFinishCountdown()
    {
        if ( currentCountdown > 0 )
        {
            --currentCountdown;
            finishCountdownTextComponent.text = "\n\n\n\n" + currentCountdown.ToString();
        }
        else
        {
            player1IdComp.playerState = EPlayerState.STATE_FOUND;
            player2IdComp.playerState = EPlayerState.STATE_FOUND;
            ChangeGameStateToMenu();
        }
    }

    void ChangeGameStateToMenu()
    {
        e_State = EGameState.STATE_MENU;
    }

    void ChangeGameStateToIdle()
    {
        player1IdComp.playerState = EPlayerState.STATE_IDLE;
        player2IdComp.playerState = EPlayerState.STATE_IDLE;
    }


    void CheckIfBallIsOutOfBounds()
    {
        if (!ballGameObject.GetComponent<BallRotation>().shouldRotate)
        {
            Vector3 ballScreenPosition = GetScreenPosition(ballGameObject.transform.position);
            bool isOnScreen = ballScreenPosition.x > 0 && ballScreenPosition.x < 1 && ballScreenPosition.y > 0.112 && ballScreenPosition.y < 0.8925;

            if (!isOnScreen)
            {
                e_State = EGameState.STATE_BALL_OUTOFFBOUNDS;
            }
        }
    }

    Vector3 GetScreenPosition(Vector3 position)
    {
        return mainCamera.WorldToViewportPoint(position);
    }

    void ReleaseBall()
    {
        ballGameObject.GetComponent<BallRotation>().shouldRotate = false;
        ballGameObject.transform.parent = null;
        GameObject playerGameObject = null;

        if (ballGameObject.GetComponent<BallBounce>().takenBy == EPlayerId.PLAYER1)
            playerGameObject = GameObject.Find("Player1");
        else if (ballGameObject.GetComponent<BallBounce>().takenBy == EPlayerId.PLAYER2)
            playerGameObject = GameObject.Find("Player2");

        if (playerGameObject)
            playerGameObject.GetComponent<PlayerGetBall>().ball = null;

        ballGameObject.GetComponent<BallBounce>().takenBy = EPlayerId.NONE;
        ballGameObject.GetComponentInChildren<Collider2D>().gameObject.layer = 8;
    }

    void CleanUpExtraBalls()
    {
       /* int powerUpBaseGroupLength = powerUpBaseGroup.Length;

        for (int i = 0; i < powerUpBaseGroupLength; i++)
        {
          //  powerUpBaseGroup[i].GetComponent<PowerUpSpawner>().ClearExtraPucks();
        }
        */
}
}
