﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputBase : MonoBehaviour
{
    public MyGameManager gameManager;

    [Header("Input")]
    public EPlayerId ePlayerId = EPlayerId.PLAYER1;
    public PlayerId playerIdComponent;

    public string horizontalAxisName = "Horizontal";
    public string verticalAxisName = "Vertical";
    public string fireButtonName = "Fire";
    public string jumpButtonName = "Jump";
    public string startButtonName = "Submit";

    public float m_fHorizontalInput = 0.0f;
    public float m_fVerticalInput = 0.0f;
    public bool m_bIsFireHold = false;
    public bool m_bIsJumpHold = false;
    public bool m_bIsStartHold = false;

    public virtual void Start()
    {
        if ( !gameManager )
        {
            gameManager = (MyGameManager) GameObject.Find("GameManager").GetComponent<MyGameManager>();
        }

        playerIdComponent = GetComponent<PlayerId>();
        ePlayerId = playerIdComponent.playerId;

        string inputSuffix = "_P";
        if ( ePlayerId == EPlayerId.PLAYER1 )
            inputSuffix += 1;
        else
            inputSuffix += 2;

        horizontalAxisName += inputSuffix;
        verticalAxisName += inputSuffix;
        fireButtonName += inputSuffix;
        jumpButtonName += inputSuffix;
        startButtonName += inputSuffix;
    }

    public virtual void Update()
    {
        GetPlayerInput(Time.deltaTime);
    }

    /// <summary>
    /// Collect input from current user
    /// </summary>
    public virtual void GetPlayerInput(float deltaTime) { }
}
